FAUSTO AXEL EVANS KEILUHU / 18215025

README

This project is about a web service that integrates a website for custom clothing with a clothing vendor.

Functional Requirements
	1.Sistem dapat mengumpulkan data pesanan baju.
		1.1 Sistem dapat mengumpulkan data pesanan baju dari berbagai website
		1.2 Sistem dapat mngumpulkan data pesanan baju secara harian
		
	2.Sistem dapat menyimpan data pesanan baju
		2.1 Sistem dapat menyimpan nama website perantara
		2.2 Sistem dapat menyimpan nama pemesan baju
		2.3 Sistem dapat menyimpan nomor telepon dari pemesan
		2.4 Sistem dapat menyimpan data ukuran baju yang dipesanan
		2.5 Sistem dapat menyimpan warna baju yang akan dipesan
		2.6 Sistem dapat menyimpan pilihan lengan panjang atau pendek
		
		
	3. Sistem dapat menampilkan data dari basis data
		3.1 Sistem dapat menampilkan nama website perantara
		3.2 Sistem dapat menampilkan nama pemesan baju
		3.3 Sistem dapat menampilkan nomor telepon dari pemesan
		3.4 Sistem dapat menampilkan data ukuran baju yang dipesanan
		3.5 Sistem dapat menampilkan warna baju yang akan dipesan
		3.6 Sistem dapat menampilkan pilihan lengan panjang atau pendek
		
		
	4. Sistem dapat secara periodik menghapus data dari database
	
	
Nonfunctional Requirements
	1. Sistem mampu menampilkan data dengan desain yang user friendly
	2. Sistem mampu menampilkan data yang diminta dengan response time tidak lebih dari tiga detik
	3. Sistem mampu memiliki kapasitas penyimpanan data yang cukup untuk menyimpan data pesanan paling tidak selama dua minggu

Hardware / Software Requirements
	1. Server
	    1.1 Sistem minimal memiliki prosesor dengan kecepatan minimal 2 GHz
		1.2 Sistem minimal memiliki RAM dengan kapasitas 2 GB RAM
		1.3 Sistem memiliki operating sistem linux tanpa GUI, operating system yang akan digunakan adalah Linux Ubuntu
	
	2. Client
		2.1 Client membutuhkan operating system dengan graphical user interface
		2.2 Client membutuhkan perangkat yang memiliki browser yang mendukung bahasa html, php, dan format json
		2.3 Client membutuhkan koneksi ke internet untuk mengakses web service ini
	
	

