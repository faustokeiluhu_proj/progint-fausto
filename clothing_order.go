package main
import (
    "database/sql"
    "encoding/json"
    "fmt"
    "log"

    "net/http"
    _ "github.com/go-sql-driver/mysql"
)

        type pemesanan_var struct {
            Id_pemesanan string
            Id_toko string
            Nama_pemesan string
            No_telp string
            Alamat_pemesan string
            Ukuran string
            Warna string
            Pilihan_lengan string
        }
        type pemesanan struct{

        //tulis di dokumentasi kalau data butuh json bentuk seperti... omitempty artinya apa..


          // MODEL
        	Id_pemesanan string `json:"Id_pemesanan, omitempty" sql:"id_pemesanan"`
        	Id_toko string `json:"Id_toko, omitempty" sql:"id_toko"`
        	Nama_pemesan string `json:"Nama_pemesan, omitempty" sql:"nama_pemesan"`
        	No_telp string `json:"No_telp, omitempty" sql:"no_telp"`
        	Alamat_pemesan string `json:"Alamat_pemesan, omitempty" sql:"alamat_pemesan"`
          Ukuran string `json:"Ukuran, omitempty" sql:"ukuran"`
          Warna string `json:"Warna, omitempty" sql:"warna"`
          Pilihan_lengan string `json:"Pilihan_lengan , omitempty" sql:"Pilihan_lengan"`
        }



func main(){
        port:=8181

        //Serve File digunakan untuk membalas request user dengan file

        http.HandleFunc("/index/", func(w http.ResponseWriter, r *http.Request){
                http.ServeFile(w,r,"index.html")
        })

        http.HandleFunc("/lihat-pemesanan/", func(w http.ResponseWriter, r *http.Request){
                http.ServeFile(w,r,"lihat-pemesanan.html")
        })

        http.HandleFunc("/update-pemesanan/", func(w http.ResponseWriter, r *http.Request){
                http.ServeFile(w,r,"update-pemesanan.html")
        })

        http.HandleFunc("/pemesanan/", func(w http.ResponseWriter, r *http.Request){
                switch r.Method{
                    case "GET":
                      FetchAllData(w,r)
                      break

                    case "POST":
                      break

              }

        })

        http.HandleFunc("/jquery-3.2.1.min.js", func(w http.ResponseWriter, r *http.Request){
                http.ServeFile(w,r,"jquery-3.2.1.min.js")
        })

        //insert-data-baru
        http.HandleFunc("/insert-data-baru/", func(w http.ResponseWriter, r *http.Request){
                http.ServeFile(w,r,"insert-data-baru.html")

        })

        //update-data
        http.HandleFunc("/update-data/", func(w http.ResponseWriter, r *http.Request){
                //http.ServeFile(w,r,"update-data.html")
                if r.Method == "POST"{
                  InsertData(w,r)

                }else {
                  http.ServeFile(w,r,"update-data.html")
                }

        })

        //delete-data
        http.HandleFunc("/delete-data/", func(w http.ResponseWriter, r *http.Request){
          //DELETE Request

                if r.Method == "GET" {
                     http.ServeFile(w,r,"delete-data.html")
                  } else {

                      s := r.URL.Path[len("/delete-data/"):]
                      DeleteData(w,r,s)
                  }


        })

        log.Printf("Server starting on port %v\n",port)
        log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v",port),nil))
}

func FetchAllData(w http.ResponseWriter, r *http.Request){
      db, err := sql.Open("mysql", "root@tcp(167.205.67.251:3306)/clothing_project")
      w.Header().Set("Content-Type", "application/json")

      if err!= nil{
          log.Fatal(err)
      }

      defer db.Close()

      pemesanan:=pemesanan_var{}

      rows,err := db.Query("select id_pemesanan, id_toko, nama_pemesan, no_telp, alamat_pemesan, ukuran, warna, pilihan_lengan from pemesanan")

      if err!=nil{
          log.Fatal(err)
      }
      defer rows.Close()

      for rows.Next(){

          err:= rows.Scan(&pemesanan.Id_pemesanan, &pemesanan.Id_toko, &pemesanan.Nama_pemesan, &pemesanan.No_telp, &pemesanan.Alamat_pemesan, &pemesanan.Ukuran, &pemesanan.Warna, &pemesanan.Pilihan_lengan)

          if err!=nil{
            log.Fatal(err)
          }

          json.NewEncoder(w).Encode(&pemesanan)

      }

      err = rows.Err()

}

func InsertData(w http.ResponseWriter, r *http.Request){
  var data_pemesanan pemesanan
  decoder := json.NewDecoder(r.Body)
  err := decoder.Decode(&data_pemesanan)


  db,err := sql.Open("mysql", "root:@tcp(167.205.67.251:3306)/clothing_project")
      if err != nil{
        log.Fatal(err)
      }

  rows,err :=db.Query("SELECT * FROM pemesanan WHERE id_pemesanan=?" , data_pemesanan.Id_pemesanan)
      if err != nil{
        log.Fatal(err)
      }

  if !rows.Next(){
    _,err =db.Query("INSERT INTO pemesanan values(?,?,?,?,?,?,?,?)" , data_pemesanan.Id_pemesanan, data_pemesanan.Id_toko, data_pemesanan.Nama_pemesan, data_pemesanan.No_telp, data_pemesanan.Alamat_pemesan, data_pemesanan.Ukuran, data_pemesanan.Warna, data_pemesanan.Pilihan_lengan)
  
        if err != nil{
          log.Fatal(err)
        }
  } else {
    _,err =db.Query("UPDATE pemesanan SET id_pemesanan=?, id_toko=?, nama_pemesan=?, no_telp=?, alamat_pemesan=?, ukuran=?, warna=?, pilihan_lengan=? WHERE id_pemesanan=?" , data_pemesanan.Id_pemesanan, data_pemesanan.Id_toko, data_pemesanan.Nama_pemesan, data_pemesanan.No_telp, data_pemesanan.Alamat_pemesan, data_pemesanan.Ukuran, data_pemesanan.Warna, data_pemesanan.Pilihan_lengan, data_pemesanan.Id_pemesanan )

        if err != nil{
          log.Fatal(err)
        }
  }


  }

  func DeleteData(w http.ResponseWriter, r *http.Request, id string){
  	db,err := sql.Open("mysql", "root:@tcp(167.205.67.251:3306)/clothing_project")
  	if err != nil{
  		log.Fatal(err)
  	}

  	rows,err := db.Query("DELETE FROM pemesanan WHERE id_pemesanan=?",id)

  	defer rows.Close()

  }
